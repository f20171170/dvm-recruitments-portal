# meant to be imported then executed after running python manage.py shell

from recportal.models import *
from django.contrib.auth.models import *

people = list()

class Person:
    def __init__(self, uname, fn, ln, team, lvl):
        self.uname = uname
        self.fn = fn
        self.ln = ln
        self.team = team
        self.lvl = lvl
        people.append(self)

    def __str__(self):
        return (self.fn + " " + self.ln)


def AddPeople():
    Person("hemanth", "Hemanth", "Alluri", "Backend", 1)
    Person("divyam", "Divyam", "Goel", "Backend", 1)
    Person("megh", "Megh", "Thakkar", "Backend", 2)
    Person("amritanshu", "Amritanshu", "Jain", "Backend", 3)
    Person("chinmay", "Chinmay", "Pandhre", "Frontend", 2)
    Person("nayan", "Nayan", "Khanna", "Backend", 1)
    Person("raghav", "Raghav", "Arora", "Backend", 1)
    Person("sanchit", "Sanchit", "Ahuja", "Backend", 1)
    Person("kps", "Kali Prasad", "Swain", "Video", 1)
    Person("yashdeep", "Yashdeep", "Gupta", "App Dev", 1)
    Person("kunal", "Kunal", "Mohta", "Frontend", 1)
    Person("sayeed", "Sayeed", "Ahmed", "Graphics", 1)
    Person("kunal", "Kunal", "Mohta", "Frontend", 1)
    Person("madhur", "Madhur", "Wadhwa", "Graphics", 2)
    Person("laksh", "Laksh", "Singla", "Frontend", 1)
    Person("rishabh_g", "Rishabh", "Goyal", "Frontend", 1)
    Person("varun", "Varun", "Kohli", "Video", 1)
    Person("yash", "Yash", "Bhagat", "App Dev", 1)
    Person("nishant", "Nishant", "Mahajan", "App Dev", 1)
    Person("vaibhav", "Vaibhav", "Maheshwari", "App Dev", 2)
    Person("abhishek", "Abhishek", "Sharma", "App Dev", 1)
    Person("tushar", "Tushar", "Goel", "Backend", 2)
    Person("rahil", "Rahil", "Jain", "Graphics", 1) 
    Person("hardik", "Hardik", "Bhatnagar", "Video", 1)
    Person("prajjwal", "Prajjwal", "Mahajan", "Video", 2)
    Person("tushar", "Tushar", "Goel", "Backend", 2)
    Person("anjali", "Anjali", "Khantal", "Frontend", 1) 
    Person("sartak", "Sartak", "Sehgal", "Frontend", 1)
    Person("rishabh_a", "Rishabh", "Arora", "Frontend", 1)
    Person("pratik", "Pratik", "Borikar", "Graphics", 1)
    Person("rajeev", "Rajeev", "Singh", "Video", 1)
    Person("aaryan", "Aaryan", "Budhiraja", "Graphics", 1)

    for p in people:
        try:
            newu = User.objects.create_user(username=p.uname, password="asdfghjkl", first_name=p.fn, last_name=p.ln, is_staff=true)
            newu_ext = Senior.objects.get(user=newu)
            newu_ext.team = p.team
            newu_ext.seniority_level = p.lvl
            newu_ext.save()
            print("%s sucessfully added." % p.uname)
        except Exception as err:
            print(err)
